FROM golang:1.6

MAINTAINER grahama

RUN go get github.com/itsabot/abot

RUN touch /etc/apt/sources.list.d/postgresql.list && echo "deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main" >> /etc/apt/sources.list.d/postgresql.list

RUN apt-get update && apt-get install -y postgresql-9.5