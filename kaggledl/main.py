from robobrowser import RoboBrowser
import argparse
import os


def read_config():
    import configparser
    config = configparser.ConfigParser()
    config.read('config')
    username = config['default']['username']
    password = config['defualt']['password']
    return username, password

def main(competition, username, password):
    os.chdir('/data')
    browser = RoboBrowser(history=True, parser="html.parser")
    base = 'https://www.kaggle.com'
    browser.open('/'.join([base, 'account/login']))

    login_form = browser.get_form(action='/account/login')
    login_form['UserName'] = username
    login_form['Password'] = password
    browser.submit_form(login_form)

    browser.open('/'.join([base, 'c', competition, 'data']))
    files = []
    for a_href in browser.get_links():
        if '.zip' in a_href.text:
            files.append(a_href)

    print('downloading: ', len(files))
    for f in files:
        request = browser.session.get(base + f.attrs['href'], stream=True)
        with open(f.attrs['name'], "wb") as zip_file:
            zip_file.write(request.content)


def run():

    parser = argparse.ArgumentParser(
        description='download kaggle datasets')
    parser.add_argument('-u', '--username', help='kaggle username')
    parser.add_argument('-p', '--password', help='kaggle password')
    parser.add_argument('-c', '--competition', help='competition url')
    args = parser.parse_args()
    if args.username:
        username = args.username
        password = args.password
    else:
        username, password = read_config()
    main(competition=args.competition, username=username, password=password)


if __name__ == '__main__':
    # running via docker with
    ## docker run -v /root/data/:/data/ kaggle python main.py -c facebook-v-predicting-check-ins -u hassiktir -p licorice
    run()
