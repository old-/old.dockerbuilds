# VERSION:        0.2
# REPO/TAG:       grahama/keras:theano
# DESCRIPTION:    keras with theano and cuda4 base image
# AUTHOR:         graham annett
# COMMENTS:
#    works with grid k2
#    changed name
# SETUP:
#
#   UBUNTU:
#   MAC:
#
# USAGE:
#

FROM grahama/theano:latest

MAINTAINER grahama <graham.annett@gmail.com>
# Keras using python3 and tensorflow

RUN pip3 install --pre --upgrade git+git://github.com/fchollet/keras.git@master

RUN mkdir ~/.keras/ && echo '{"epsilon": 1e-07, "floatx": "float32", "backend": "theano"}' > ~/.keras/keras.json

WORKDIR /root/