# VERSION:        0.2
# REPO/TAG:       grahama/keras:tf
# DESCRIPTION:    keras with tensorflow base image
# AUTHOR:         graham annett
# COMMENTS:
#    works with grid k2
#    changed name
#    installs latest possible Keras
# SETUP:
#
#   UBUNTU:
#   MAC:
#
# USAGE:
#
FROM grahama/tensorflow:latest

MAINTAINER grahama <graham.annett@gmail.com>

RUN pip3 install --pre --upgrade git+git://github.com/fchollet/keras.git@master

RUN mkdir ~/.keras/ && echo '{"epsilon": 1e-07, "floatx": "float32", "backend": "tensorflow"}' > ~/.keras/keras.json

WORKDIR /root/