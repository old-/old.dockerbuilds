# VERSION:        0.2
# REPO/TAG:       grahama/tensorflow:serving-slim
# DESCRIPTION:    serving slim
# AUTHOR:         graham annett
# COMMENTS:
#    works with grid k2
#    uses python2 since dependent on grpcio stuff
#    from
# https://github.com/tensorflow/serving/blob/master/tensorflow_serving/tools/docker/Dockerfile.devel
# tutorial:
#  https://tensorflow.github.io/serving/serving_inception.html
# SETUP:
#
#   UBUNTU:
#   MAC:
#
# USAGE:
#   DOES NOT USE GPU/NVIDIA STUFF

FROM ubuntu:14.04

MAINTAINER grahama <graham.annett@gmail.com>

# environment variables for bazel
ENV BAZELRC /root/.bazelrc
ENV BAZEL_VERSION 0.2.0
ENV TF_NEED_CUDA 0
ENV PYTHON_BIN_PATH /usr/bin/python

RUN apt-get update && apt-get install -y \
        build-essential \
        curl \
        git \
        libfreetype6-dev \
        libpng12-dev \
        libzmq3-dev \
        pkg-config \
        python-dev \
        python-numpy \
        python-pip \
        software-properties-common \
        swig \
        zip \
        zlib1g-dev \
        && \
    add-apt-repository -y ppa:openjdk-r/ppa && \
    apt-get update && \
    apt-get install -y \
        openjdk-8-jdk \
        openjdk-8-jre-headless \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN curl https://bootstrap.pypa.io/get-pip.py | python - && \
    pip install --pre enum34 futures six protobuf grpcio


RUN mkdir /bazel && \
    cd /bazel && \
    curl -fSsL -O https://github.com/bazelbuild/bazel/releases/download/$BAZEL_VERSION/bazel-$BAZEL_VERSION-installer-linux-x86_64.sh && \
    curl -fSsL -o /bazel/LICENSE.txt https://raw.githubusercontent.com/bazelbuild/bazel/master/LICENSE.txt && \
    chmod +x bazel-*.sh && \
    echo "startup --batch" >>/root/.bazelrc && \
    echo "build --spawn_strategy=standalone --genrule_strategy=standalone" >>/root/.bazelrc && \
    ./bazel-$BAZEL_VERSION-installer-linux-x86_64.sh && \
    rm -f /bazel/bazel-$BAZEL_VERSION-installer-linux-x86_64.sh

WORKDIR /

CMD ["/bin/bash"]