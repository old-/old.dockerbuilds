#!/bin/sh

# cuda:base using:
# CUDA 7.5
# cudnnv4
docker build -t grahama/cuda:base -f base.Dockerfile .
docker build -t grahama/cuda:cudnn5 -f cudnn5.Dockerfile .

# tensorflow and theano builds
docker build -t grahama/tensorflow:latest -f tensorflow/Dockerfile tensorflow/
docker build -t grahama/theano:latest -f theano/Dockerfile theano/

# keras builds "build -t" -> "build --no-cache -t" for latest/errors
docker build -t grahama/keras:tf -f keras/keras-tf.Dockerfile keras/
docker build -t grahama/keras:theano -f keras/keras-theano.Dockerfile keras/

docker build -t grahama/parsey -f parsey/Dockerfile parsey/

# building tensorflow serving and serving slim
# docker build -t grahama/tensorflow:serving --no-cache -f tensorflow/serving.Dockerfile tensorflow/
# docker build -t grahama/tensorflow:slim-serving --no-cache  -f tensorflow/slim-serving.Dockerfile tensorflow/

# gym builds
# docker build -t grahama/gym:tf -f gym/gym.Dockerfile gym/