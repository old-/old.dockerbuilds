# running with gpu

to run these with gpu requires nvidia-docker-plugin and nvidia-docker with NV_GPU='0,1' type command, i.e.

```
# Running nvidia-docker isolating specific GPUs
NV_GPU='0,1' nvidia-docker <docker-options> <docker-command> <docker-args>
```

order of builds

1. base
2. from base -> tf + theano
3. from tf or theano -> keras