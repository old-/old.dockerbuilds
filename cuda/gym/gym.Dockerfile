# VERSION:        0.1
# REPO/TAG:       grahama/gym:tf
# DESCRIPTION:    gym cuda capable with keras
# AUTHOR:         graham annett
# COMMENTS:
#     gym environment
# SETUP:
#
#   UBUNTU:
#   MAC:
#
# USAGE:
#   cd

FROM grahama/keras:tf

# fix this later,
RUN apt-get clean && mv /var/lib/apt/lists /tmp && mkdir -p /var/lib/apt/lists/partial && apt-get clean

RUN apt-get update && \
    apt-get install -y \
        cmake \
        zlib1g-dev \
        libjpeg-dev \
        xvfb \
        libav-tools \
        xorg-dev  \
        python3-opengl

RUN git clone https://github.com/openai/gym.git && \
    cd gym && \
    pip3 install -e '.[all]'