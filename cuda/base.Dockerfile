# VERSION:        0.2
# REPO/TAG:       grahama/cuda:base
# DESCRIPTION:    base cuda4 with CuDNN image, lots of consolidation to minimize image size
# AUTHOR:         graham annett
# COMMENTS:
#    works with grid k2
# SETUP:
#
#   UBUNTU:
#   MAC:
#
# USAGE:
#
FROM nvidia/cuda:7.5-cudnn4-devel

MAINTAINER grahama <graham.annett@gmail.com>

RUN apt-get update && apt-get install -y \
        software-properties-common \
        build-essential \
        curl \
        git \
        wget \
        libfreetype6-dev \
        libpng12-dev \
        libzmq3-dev \
        pkg-config \
        python3 \
        python3-dev \
        python3-numpy \
        python3-scipy \
        swig \
        zip \
        zlib1g-dev \
        libhdf5-dev \
        libyaml-dev \
        libjpeg-dev \
        gfortran \
        libopenblas-dev \
        liblapack-dev \
        libhdf5-dev \
        libjpeg-dev \
        vim \
        unzip \
        cmake \
        libatlas-base-dev \
        libjasper-dev \
        libgtk2.0-dev \
        libavcodec-dev \
        libavformat-dev \
        libswscale-dev \
        libjpeg-dev \
        libpng-dev \
        libtiff-dev \
        libjasper-dev \
        libv4l-dev \
        && \
    apt-get clean

RUN curl -O https://bootstrap.pypa.io/get-pip.py && \
        python3 get-pip.py && \
        rm get-pip.py

RUN pip3 install --pre \
        ipykernel \
        jupyter \
        matplotlib \
        cython \
        && \
    python3 -m ipykernel.kernelspec && \
    pip3 install h5py Pillow && \
    pip3 install --pre scikit-image

RUN mkdir ~/opencv && \
    cd ~/opencv && \
    git clone https://github.com/Itseez/opencv_contrib.git && \
    cd opencv_contrib && \
    git checkout 3.1.0 && \
    cd ~/opencv && \
    git clone https://github.com/Itseez/opencv.git && \
    cd opencv && \
    git checkout 3.1.0 && \
    cd ~/opencv/opencv && mkdir release && cd release && \
          cmake -D CMAKE_BUILD_TYPE=RELEASE \
          -D CMAKE_INSTALL_PREFIX=/usr/local \
          -D WITH_CUDA=OFF \
          -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules \
          -D INSTALL_C_EXAMPLES=ON \
          -D INSTALL_PYTHON_EXAMPLES=ON \
          -D BUILD_EXAMPLES=ON \
          -D WITH_OPENGL=ON \
          -D WITH_V4L=ON \
          -D WITH_XINE=ON \
          -D WITH_TBB=ON .. &&\
    cd ~/opencv/opencv/release && make -j $(nproc) && make install && \
    rm -rf ~/opencv

# Set the locale
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

WORKDIR /root/