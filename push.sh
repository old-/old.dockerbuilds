#!/bin/sh

docker push grahama/keras:theano
docker push grahama/keras:tf
docker push grahama/cuda:theano
docker push grahama/cuda:tf
docker push grahama/cuda:base
docker push grahama/parsey